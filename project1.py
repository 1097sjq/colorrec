# -*- coding: utf-8 -*-
"""
Created on Mon Dec  5 20:48:46 2022

@author: 86139
"""


import cv2
import numpy as np
def cv_show(name,img):
    cv2.imshow(name,img)
    cv2.waitKey()
    cv2.destroyAllWindows()#展示图片
img = cv2.imread(r"origin.png")
kernel=np.ones((5,5),np.uint8)
hsv=cv2.cvtColor(img,cv2.COLOR_BGR2HSV)#BGR转为HSV
lower_red=np.array([0,137,0])#红色阈值下界
higher_red=np.array([10,255,255])#红色阈值上界
mask=cv2.inRange(hsv,lower_red,higher_red)
mask=cv2.medianBlur(mask,15)
mask=cv2.medianBlur(mask,15)
mask=cv2.medianBlur(mask,15)#中值滤波平滑红球
mask=cv2.dilate(mask,kernel,iterations=1)
mask=cv2.dilate(mask,kernel,iterations=1)
mask=cv2.dilate(mask,kernel,iterations=1)#适当膨胀操作
ret,thresh=cv2.threshold(mask,127,255,cv2.THRESH_BINARY)#处理后图像变为二值图像
contours,hierarchy=cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
#获得轮廓
cnt=contours[0]
x,y,w,h=cv2.boundingRect(cnt)#轮廓分解
rect=cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)#画出外接矩形
font = cv2.FONT_HERSHEY_PLAIN
text="("+str(x)+","+str(y)+")"
cv2.putText(img, text,(x-40, y-5), font, 1, (0, 255, 0), 1)#标上坐标信息
cv_show("rect",rect)
cv2.imwrite("result.png",img)